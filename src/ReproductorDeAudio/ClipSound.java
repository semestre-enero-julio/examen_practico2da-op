package ReproductorDeAudio;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.util.concurrent.TimeUnit;


/**
 *
 * @author Dreed
 */
public class ClipSound implements LineListener {

    public boolean playCompleted;
    public Clip audioClip;
    public long clipTime;

    public ClipSound() {                                                        
        playCompleted = true;
    }

    public long getClipTime() {                                               
        return clipTime;
    }

    public void setClipTime(long clipTime) {                                    
        this.clipTime = clipTime;
    }

    public Clip getAudioClip() {                                              
        return audioClip;
    }

    public void setAudioClip(Clip audioClip) {                                
        this.audioClip = audioClip;
    }

    public String getStatusClip() {                                                                                                                             
        return getDuration(getAudioClip().getMicrosecondPosition());            
    }

    public String getStatusClipAll() {
        return getDuration(getAudioClip().getMicrosecondLength());             
    }

    public static String getDuration(long millis) {                           
        int seconds = (int) TimeUnit.MICROSECONDS.toSeconds(millis);            
        return secondsToTime(seconds);
    }

    public static String secondsToTime(int secondsTime) {                     
        int hours = (secondsTime / 3600);
        int minutes = ((secondsTime - hours * 3600) / 60);
        int seconds = secondsTime - (hours * 3600 + minutes * 60);
        return addCeros(hours) + ":" + addCeros(minutes) + ":" + addCeros(seconds);
    }

    public static String addCeros(int time) {                                  
        String timeS = String.valueOf(time);                                    
        if (timeS.length() == 1) {                                            
            String tmp = timeS;
            timeS = " ";
            timeS = "0".concat(tmp);
            return timeS;
        } else {
            return timeS;
        }

    }
    public int maxiumSeconds(long millis) {                                      
        return (int) TimeUnit.MICROSECONDS.toSeconds(millis);                   
    }

    public void play() {                                                              
        getAudioClip().start();
    }

    public void pause() {                                                       
        if (this.playCompleted == false) {
            getAudioClip().setMicrosecondPosition(getClipTime());                      
            getAudioClip().start();
            this.playCompleted = true;
        } else {
            setClipTime(getAudioClip().getMicrosecondPosition());
            getAudioClip().stop();
            this.playCompleted = false;
        }
    }

    public void stop() {                                                      
        getAudioClip().close();
        System.exit(0);
    }

    public void openClip(String audioFilePath) {                                
        System.out.println(">> Abrir AudioClip");
        File audioFile = new File(audioFilePath);
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            setAudioClip((Clip) AudioSystem.getLine(info));
            getAudioClip().addLineListener(this);
            getAudioClip().open(audioStream);

        } catch (UnsupportedAudioFileException ex) {
            System.out.println("Archivo De Audio No Soportado");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Linea De Audio Incorrecta");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }

    }

    @Override
    public void update(LineEvent event) {                                      
        LineEvent.Type type = event.getType();
        if (type == LineEvent.Type.START) {
            System.out.println(">> Play AudioClip");
        } else if (type == LineEvent.Type.STOP) {
            System.out.println(">> Pause AudioClip");
        } else if (type == LineEvent.Type.CLOSE) {
            System.out.println(">> Parar AudioClip");
        }
    }
}
