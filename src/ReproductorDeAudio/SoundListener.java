package ReproductorDeAudio;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSlider;

/**
 *
 * @author Dreed
 */

public class SoundListener implements ActionListener {

    private final PanelVista view;                                              
    private final ClipSound model;                                              
    private Thread thread;                                                      
    private int timeClip;                                                       
    private boolean threadStatus;                                              

    public SoundListener(PanelVista view, ClipSound model) {
        this.view = view;
        this.model = model;
        this.threadStatus = true;
    }

    public Thread getThread() {                                                 
        return thread;
    }

    public void setThread(Thread thread) {                                      
        this.thread = thread;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Component component = (Component) e.getSource();
        switch (component.getName()) {
            case "Abrir":
                JFileChooser seleccion = new JFileChooser();
                int opcion1 = seleccion.showOpenDialog(view);
                if (opcion1 == JFileChooser.APPROVE_OPTION) {                   
                    String audioFilePath = seleccion.getSelectedFile().getPath();
                    view.setPath(audioFilePath);
                    model.openClip(audioFilePath);
                    model.play();
                    view.setEndClipTime(model.getStatusClipAll());            
                    view.getSlider().setMaximum(model.maxiumSeconds(model.getAudioClip().getMicrosecondLength())); 
                    setThread(new ThreadCounter(view.getSlider()));
                    getThread().start();
                    view.setOpenButton("CANCELADO");                            
                }
                break;
            case "pause":
                model.pause();
                this.threadStatus = model.playCompleted;
                if (!getThread().isAlive()) {                                  
                    setThread(new ThreadCounter(view.getSlider()));             
                    getThread().start();
                }
                break;
            case "stop":
                model.stop();
                view.setOpenButton("Abrir");                                     
                break;
            case "CANCELADO":
                JOptionPane.showMessageDialog(view, "Se esta reproducionedo una audio",
                        "Error", JOptionPane.OK_OPTION);
                break;
        }
    }

    public class ThreadCounter extends Thread {

        private final JSlider bar;

        public ThreadCounter(JSlider bar) {
            this.bar = bar;
        }

        @SuppressWarnings("WaitWhileNotSynced")
        public void toPrint() {
            for (int i = bar.getMinimum(); i <= bar.getMaximum(); i++) {
                if (threadStatus == true) {                                     
                    view.setStartClipTime(model.getStatusClip());
                    bar.setValue(timeClip + i);
                    view.repaint();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        System.out.println("Interruccion Error" + ex);
                    }
                    Toolkit.getDefaultToolkit().sync();                           
                } else {
                    timeClip = model.maxiumSeconds(model.getAudioClip().getMicrosecondPosition());
                    view.getSlider().setValue(timeClip);
                    try {                                                        
                        do {                                                   
                            wait();
                        } while (!threadStatus);
                    } catch (InterruptedException ex) {
                        System.out.println("Exception: " + ex);
                    }
                    break;
                }

            }
        }

        @Override
        public void run() {
            toPrint();
        }
    }
}
