
package ReproductorDeAudio;


import javax.swing.JFrame;

/**
 *
 * @author Dreed
 */

class SoundControl {

    public static void main(String[] args) {
        creaFrame();
    }

    static void creaFrame() {
        ClipSound modelSound = new ClipSound();                                 
        PanelVista panelSound = new PanelVista(modelSound);                    
        SoundListener listenerSound = new SoundListener(panelSound,modelSound); 
        panelSound.addEvents(listenerSound);                                   
        
        JFrame frameSound = new JFrame("Java Reproductor De Audio");
        frameSound.setSize(700, 200);
        frameSound.setLocation(100, 100);
        frameSound.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frameSound.add(panelSound);
        frameSound.setVisible(true);
    }
}
