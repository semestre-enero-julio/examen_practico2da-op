/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JuegoDelGato;

import java.util.Random;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author Dreed
 */
public class JugadorVsMaquina extends javax.swing.JFrame {

    public int Turno = 1;
    public JButton mov[] = new JButton[9];
   

    public JugadorVsMaquina() {
        initComponents();

        mov[0] = (JButton) add(Casilla1_);
        mov[1] = (JButton) add(Casilla2_);
        mov[2] = (JButton) add(Casilla3_);
        mov[3] = (JButton) add(Casilla4_);
        mov[4] = (JButton) add(Casilla5_);
        mov[5] = (JButton) add(Casilla6_);
        mov[6] = (JButton) add(Casilla7_);
        mov[7] = (JButton) add(Casilla8_);
        mov[8] = (JButton) add(Casilla9_);
    }

    public void Posiciones() {
        Ganador(Casilla1_, Casilla2_, Casilla3_);
        Ganador(Casilla4_, Casilla5_, Casilla6_);
        Ganador(Casilla7_, Casilla8_, Casilla9_);

        Ganador(Casilla1_, Casilla4_, Casilla7_);
        Ganador(Casilla2_, Casilla5_, Casilla8_);
        Ganador(Casilla3_, Casilla6_, Casilla9_);

        Ganador(Casilla1_, Casilla5_, Casilla9_);
        Ganador(Casilla3_, Casilla5_, Casilla7_);

        Empate(Casilla1_, Casilla2_, Casilla3_, Casilla4_, Casilla5_, Casilla6_, Casilla7_, Casilla8_, Casilla9_);

    }

    public void Ganador(JButton C1, JButton C2, JButton C3) {

        if (C1.getText() == "X" && C2.getText() == "X" && C3.getText() == "X") {

            JOptionPane.showMessageDialog(null, "El Ganador Es La IA-(X)");
            NuevoJuego();

        } else if (C1.getText() == "O" && C2.getText() == "O" && C3.getText() == "O") {

            JOptionPane.showMessageDialog(null, "El Ganador Es El Humano-(O)");
            NuevoJuego();

        }

    }

    public void Empate(JButton C1, JButton C2, JButton C3, JButton C4, JButton C5, JButton C6, JButton C7, JButton C8, JButton C9) {

        if (Casilla1_.getText() != "" && Casilla2_.getText() != "" && Casilla3_.getText() != "" && Casilla4_.getText() != "" && Casilla5_.getText() != "" && Casilla6_.getText() != "" && Casilla7_.getText()
                != "" && Casilla8_.getText() != "" && Casilla9_.getText() != "") {

            JOptionPane.showMessageDialog(null, "El Juego Esta Empatado");
            NuevoJuego();
        }

    }

    public void IA() {
        if (Turno == 1) {
            Jugadas(Casilla1_, Casilla2_, Casilla3_);
        }
        if (Turno == 1) {
            Jugadas(Casilla4_, Casilla5_, Casilla6_);
        }
        if (Turno == 1) {
            Jugadas(Casilla7_, Casilla8_, Casilla9_);
        }

        if (Turno == 1) {
            Jugadas(Casilla1_, Casilla4_, Casilla7_);
        }
        if (Turno == 1) {
            Jugadas(Casilla2_, Casilla5_, Casilla8_);
        }
        if (Turno == 1) {
            Jugadas(Casilla3_, Casilla6_, Casilla9_);
        }

        if (Turno == 1) {
            Jugadas(Casilla1_, Casilla5_, Casilla9_);
        }

        if (Turno == 1) {
            Jugadas(Casilla3_, Casilla5_, Casilla7_);
        }
        if (Turno == 1) {

            Random random = new Random();
            int num;
            boolean estado = true;
            do {

                num = (int)(random.nextDouble()*9+1);
                if (mov[num - 1].getText() == "") {
                    mov[num - 1].setText("X");
                    estado = false;
                }

            } while (estado);

        }

    }

    public void Jugadas(JButton C1, JButton C2, JButton C3) {

        if (C1.getText() == "O" && C2.getText() == "O" && C2.getText() == "") {
            C3.setText("X");
            this.Turno = 2;

        } else if (C1.getText() == "O" && C2.getText() == "" && C2.getText() == "O") {
            C2.setText("X");
            this.Turno = 2;

        } else if (C1.getText() == "" && C2.getText() == "O" && C2.getText() == "O") {
            C1.setText("X");
            this.Turno = 2;

        } else if (C1.getText() == "X" && C2.getText() == "X" && C2.getText() == "") {
            C3.setText("X");
            this.Turno = 2;

        } else if (C1.getText() == "X" && C2.getText() == "" && C2.getText() == "X") {
            C2.setText("X");
            this.Turno = 2;

        } else if (C1.getText() == "" && C2.getText() == "X" && C2.getText() == "X") {
            C1.setText("X");
            this.Turno = 2;

        }

    }

    public void NuevoJuego() {

        Casilla1_.setText("");
        Casilla2_.setText("");
        Casilla3_.setText("");
        Casilla4_.setText("");
        Casilla5_.setText("");
        Casilla6_.setText("");
        Casilla7_.setText("");
        Casilla8_.setText("");
        Casilla9_.setText("");
        this.Turno = 1;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Casilla4_ = new javax.swing.JButton();
        Casilla7_ = new javax.swing.JButton();
        Casilla5_ = new javax.swing.JButton();
        Casilla6_ = new javax.swing.JButton();
        Casilla8_ = new javax.swing.JButton();
        Casilla9_ = new javax.swing.JButton();
        Casilla1_ = new javax.swing.JButton();
        Casilla2_ = new javax.swing.JButton();
        Casilla3_ = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Casilla4_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla4_ActionPerformed(evt);
            }
        });

        Casilla7_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla7_ActionPerformed(evt);
            }
        });

        Casilla5_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla5_ActionPerformed(evt);
            }
        });

        Casilla6_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla6_ActionPerformed(evt);
            }
        });

        Casilla8_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla8_ActionPerformed(evt);
            }
        });

        Casilla9_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla9_ActionPerformed(evt);
            }
        });

        Casilla1_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla1_ActionPerformed(evt);
            }
        });

        Casilla2_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla2_ActionPerformed(evt);
            }
        });

        Casilla3_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Casilla3_ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Casilla4_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla1_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla7_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Casilla5_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Casilla6_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Casilla2_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Casilla3_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Casilla8_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Casilla9_, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Casilla1_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla2_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla3_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Casilla4_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla5_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla6_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Casilla7_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla8_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Casilla9_, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Casilla4_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla4_ActionPerformed

         if (Casilla4_.getText() == "") {

            Casilla4_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();
    }//GEN-LAST:event_Casilla4_ActionPerformed

    private void Casilla7_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla7_ActionPerformed

     if (Casilla7_.getText() == "") {

            Casilla7_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();

    }//GEN-LAST:event_Casilla7_ActionPerformed

    private void Casilla5_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla5_ActionPerformed

         if (Casilla5_.getText() == "") {

            Casilla5_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();

    }//GEN-LAST:event_Casilla5_ActionPerformed

    private void Casilla6_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla6_ActionPerformed

        if (Casilla6_.getText() == "") {

            Casilla6_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();

    }//GEN-LAST:event_Casilla6_ActionPerformed

    private void Casilla8_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla8_ActionPerformed

        if (Casilla8_.getText() == "") {

            Casilla8_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();

    }//GEN-LAST:event_Casilla8_ActionPerformed

    private void Casilla9_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla9_ActionPerformed

         if (Casilla9_.getText() == "") {

            Casilla9_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();
    }//GEN-LAST:event_Casilla9_ActionPerformed

    private void Casilla1_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla1_ActionPerformed

        if (Casilla1_.getText() == "") {

            Casilla1_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();
      
    }//GEN-LAST:event_Casilla1_ActionPerformed

    private void Casilla2_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla2_ActionPerformed

        if (Casilla2_.getText() == "") {

            Casilla2_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();

       
    }//GEN-LAST:event_Casilla2_ActionPerformed

    private void Casilla3_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Casilla3_ActionPerformed

        if (Casilla3_.getText() == "") {

            Casilla3_.setText("O");
            this.Turno = 1;

        }
        IA();
        Posiciones();
    }//GEN-LAST:event_Casilla3_ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JugadorVsMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JugadorVsMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JugadorVsMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JugadorVsMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JugadorVsMaquina().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Casilla1_;
    private javax.swing.JButton Casilla2_;
    private javax.swing.JButton Casilla3_;
    private javax.swing.JButton Casilla4_;
    private javax.swing.JButton Casilla5_;
    private javax.swing.JButton Casilla6_;
    private javax.swing.JButton Casilla7_;
    private javax.swing.JButton Casilla8_;
    private javax.swing.JButton Casilla9_;
    // End of variables declaration//GEN-END:variables
}
